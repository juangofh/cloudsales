package com.sales.views.activities;

import android.app.ActionBar;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sales.R;
import com.sales.db.SqliteClass;
import com.sales.models.CustomerLocationClass;
import com.sales.utils.Util;

import java.util.ArrayList;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap map;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        context = this;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home){
            finish();
        }
        else if(id==R.id.action_logout){
            Util.logout(MapsActivity.this, context);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        LatLng customer = null;
        ArrayList<CustomerLocationClass> test = SqliteClass.getInstance(getApplicationContext()).databasehelp.customersql.getCustomersLocation();
        for (int i = 0; i<test.size(); i++){
            float lat = Float.parseFloat(test.get(i).getLatitude());
            float lon = Float.parseFloat(test.get(i).getLongitude());
            customer = new LatLng(lat, lon);
            map.addMarker(new MarkerOptions().position(customer).title(test.get(i).getName()));
        }
        map.moveCamera(CameraUpdateFactory.newLatLng(customer));
    }


}
