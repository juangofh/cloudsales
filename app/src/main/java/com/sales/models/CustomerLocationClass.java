package com.sales.models;

public class CustomerLocationClass {

    public int id;
    public String code;
    public String name;
    public String ruc;
    public String address;
    public String representative;
    public String day;
    public String phone;
    public String mobilePhone;
    public String latitude;
    public String longitude;

    public CustomerLocationClass() {
    }

    public CustomerLocationClass(
            int id,
            String code,
            String name,
            String ruc,
            String address,
            String representative,
            String day,
            String phone,
            String mobilePhone,
            String latitude,
            String longitude
    ) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.ruc = ruc;
        this.address = address;
        this.representative = representative;
        this.day = day;
        this.phone = phone;
        this.mobilePhone = mobilePhone;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRepresentative() {
        return representative;
    }

    public void setRepresentative(String representative) {
        this.representative = representative;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
